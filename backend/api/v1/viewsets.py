from rest_framework import viewsets, mixins, filters, permissions, status
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from shop.models import Category, Product, Customer, Order, OrderItem
from .serializers import CategorySerialzier, ProductSerialzer, CustomerSerializer, OrderSerializer, OrderItemSerializer,\
                        ProductCreateSerializer, OrderCreateSerializer, OrderItemCreateSerializer

from .utils import Pagination

class CategoryViewsets(viewsets.GenericViewSet,
               mixins.ListModelMixin):
    
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['name']
    ordering_fields = ['name']
    
    def get_permissions(self):
        return [permissions.IsAuthenticated()]
    
    def get_queryset(self):
        return Category.objects.filter(is_use=True)
    
    def get_serializer_class(self):
        return CategorySerialzier


class ProductViewsets(viewsets.GenericViewSet,
                      mixins.ListModelMixin,
                      mixins.CreateModelMixin):
    
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['category']
    search_fields = ['name']
    ordering_fields = ['id', 'price']
    pagination_class = Pagination
    
    def get_permissions(self):
        return [permissions.IsAuthenticated()]
    
    def get_queryset(self):
        return Product.objects.filter(is_use=True)
    
    def get_serializer_class(self):
        if self.action == 'create':
            return ProductCreateSerializer
        return ProductSerialzer
    
    def create(self, request, *args, **kwargs):
        serializer = ProductCreateSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.validated_data
            if Product.objects.filter(name=data['name']).exists():
                return Response({
                    'msg': f"Product name {data['name']} does exists"
                }, status=status.HTTP_400_BAD_REQUEST)
            data.is_use = True
            Product.objects.create(**data)
            return Response({'msg': f"Product {data['name']} created"}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CustomerViewsets(viewsets.GenericViewSet,
                      mixins.ListModelMixin,
                      mixins.CreateModelMixin):
    
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['first_name', 'last_name']
    ordering_fields = ['first_name', 'last_name']
    pagination_class = Pagination
    
    def get_permissions(self):
        return [permissions.IsAuthenticated()]
    
    def get_queryset(self):
        return Customer.objects.all()
    
    def get_serializer_class(self):
        return CustomerSerializer

class OrderViewsets(viewsets.GenericViewSet,
                      mixins.ListModelMixin,
                      mixins.CreateModelMixin,
                      mixins.RetrieveModelMixin):
    
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = ['customer']
    ordering_fields = ['id', 'timestamp']
    pagination_class = Pagination
    
    def get_permissions(self):
        return [permissions.IsAuthenticated()]
    
    def get_queryset(self):
        return Order.objects.all()
    
    def get_serializer_class(self):
        if self.action == "create":
            return OrderCreateSerializer
        return OrderSerializer
    
    def retrieve(self, request, *args, **kwargs):
        obj = self.get_object()
        serializer = OrderSerializer(obj)
        response = serializer.data
        response.update({
            'item': OrderItemSerializer(OrderItem.objects.filter(order=obj), many=True).data
        })
        return Response(response, status=status.HTTP_200_OK)

class OrderItemViewsets(viewsets.GenericViewSet,
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin):
    
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = ['order']
    ordering_fields = ['id', 'order__timestamp']
    pagination_class = Pagination
    
    def get_permissions(self):
        return [permissions.IsAuthenticated()]
    
    def get_queryset(self):
        return OrderItem.objects.all()
    
    def get_serializer_class(self):
        if self.action == "create":
            return OrderItemCreateSerializer
        return OrderItemSerializer