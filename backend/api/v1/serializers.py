from rest_framework import serializers
from shop.models import Category, Product, Customer, Order, OrderItem
class CategorySerialzier(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class ProductSerialzer(serializers.ModelSerializer):
    category = CategorySerialzier()
    class Meta:
        model = Product
        fields = '__all__'

class ProductCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        exclude = ['is_use']

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'

class OrderSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer()
    class Meta:
        model = Order
        fields = '__all__'
class OrderCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'
class OrderItemSerializer(serializers.ModelSerializer):
    # order = OrderSerializer()
    product = ProductSerialzer()
    class Meta:
        model = OrderItem
        fields = '__all__'

class OrderItemCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = '__all__'