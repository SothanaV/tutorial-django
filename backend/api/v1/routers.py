from rest_framework import routers
import api.v1.viewsets as viewsets

router = routers.DefaultRouter()

router.register('category', viewsets.CategoryViewsets, basename='category')
router.register('product', viewsets.ProductViewsets, basename='product')
router.register('customer', viewsets.CustomerViewsets, basename='customer')
router.register('order', viewsets.OrderViewsets, basename='order')
router.register('order-item', viewsets.OrderItemViewsets, basename='order-item')