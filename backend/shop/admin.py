from django.contrib import admin
import shop.models as models
# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.Category._meta.fields]
    search_fields = ['name']
    list_editable = ['is_use']
admin.site.register(models.Category, CategoryAdmin)

class ProductAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.Product._meta.fields]
    list_editable = ['is_use']
    search_fields = ['name', 'category__name']
    autocomplete_fields = ['category']
admin.site.register(models.Product, ProductAdmin)

class CustomerAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.Customer._meta.fields]
    search_fields = ['first_name', 'last_name', 'tel']
admin.site.register(models.Customer, CustomerAdmin)

class OrderAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.Order._meta.fields]
    search_fields = ['customer__first_name', 'customer__last_name', 'customer__tel']
    autocomplete_fields = ['customer']
admin.site.register(models.Order, OrderAdmin)

class OrderItemAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.OrderItem._meta.fields]
    autocomplete_fields=['order', 'product']
    search_fields = ['order__customer__first_name', 'order__customer__last_name', 'product__name', 'product__category__name']
admin.site.register(models.OrderItem, OrderItemAdmin)
